package api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import devices.Device;
import devices.DevicesList;
import modes.TimeMode;
import modes.WeatherMode;
import objects.Mode;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class APIConnection {

    public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    //private static final String apiUrl = "http://sls-api.azurewebsites.net/";
    //private static final String apiUrl = "http://192.168.0.15:5000";
    //private static final String apiUrl = "http://192.168.43.0:5000";
    private String apiUrl = null;
    //private static final String apiUrl = "http://192.168.1.100:5000";


    public APIConnection() {
        apiUrl = IPAddress.getInstance().getIpAddress();
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    OkHttpClient client = new OkHttpClient();


    public boolean checkConnection(Context context){

        apiUrl = IPAddress.getInstance().getIpAddress();
        boolean success = false;

        if(apiUrl != null){
            String json  = get(apiUrl);
            if(json != null){
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String appName = jsonObject.getString("app");
                    if(appName.contentEquals("SmartLightningSystemApi"))
                        success = true;
                } catch (JSONException e) {
                    //e.printStackTrace();
                }
            }
        }

        return success;
    }

    //Wysłanie zapytania na serwer i odebranie odpowiedzi
    public String post(String json, String url) {
        if(apiUrl==null) return null;
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder().url(url).post(body).build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.isSuccessful()) {
                return "Success";
            } else {
                return "Error: " + response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Connection failed :(";
    }

    public String get(String url) {
        if(apiUrl==null) return null;
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "application/json; charset=utf-8")
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            if(response.isSuccessful()){
                return response.body().string();
            }else
                return null;
        } catch (IOException e) {
            Log.e("Error","GET Server Response Problem");
        }
        return null;
    }

    //wysłanie danych do serwera i zwrot odpowiedzi
    public String postAndGetResponse(String json, String url) {
        if(apiUrl==null) return null;
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder().url(url).post(body).build();
        Response response;
        try {
            response = client.newCall(request).execute();
            if(response.isSuccessful()) {
                return "Mode activated: " + response.body().string();
            } else {
                return "Error: " + response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Something went wrong...";
    }


    //wygenerowanie jsona z wybranym profilem i wysłanie do API
    public String setMode(Device device){
        String json = device.modeToJSON();
        return postAndGetResponse(json, String.format("%s/mode", apiUrl));
    }

    //wygenerowanie jsona z danymi godzinowymi i wysłanie do API
    public String setTimeMode(Device device){
        int id = device.getId();
        String json = device.getTime().toJSON(id);
        return post(json, String.format("%s/time", apiUrl));
    }

    public String getWeatherMode(){
        String json = get(String.format("%s/Weather", apiUrl));
        return json;
    }

    public String setManualMode(Device device){
        int id = device.getId();
        String json = device.getManualMode().toJSON(id);
        return post(json, String.format("%s/Manual", apiUrl));
    }

    public String setPartyMode(Device device){
        int id = device.getId();
        String json = device.getPartyMode().toJSON(id);
        return post(json, String.format("%s/Flow", apiUrl));
    }

    public String setMotionMode(Device device){
        int id = device.getId();
        String json = device.getMotionMode().toJSON(id);
        return post(json, String.format("%s/Motion", apiUrl));
    }

    public String setActivityMode(Device device){
        int id = device.getId();
        String json = device.getActivityMode().toJSON(id);
        return post(json, String.format("%s/Activity", apiUrl));
    }

    public String getDevicesList(){
        String json = get(String.format("%s/Devices", apiUrl));
        return json;
    }

    public String getDeviceData(int id){
        String json = get(String.format("%s/Devices/", apiUrl)+ String.valueOf(id));
        return  json;
    }

    public String setAcuAdress(){
        String json = IPAddress.getInstance().toJSON();
        return post(json, String.format("%s/ip/acu", apiUrl));
    }

    public String setDeviceName(Device device){
        String json = device.nameToJSON();
        return post(json, String.format("%s/Devices", apiUrl));
    }





}
