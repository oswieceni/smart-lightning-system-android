package api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

/**
 * Holds IP address
 */

public class IPAddress implements Serializable {

    private String ipAddress = null;
    private String ip = null;
    private String port = null;
    private int method = -1;
    private String acu_ip = null;
    private int acu_id = -1;
    private static final IPAddress ourInstance = new IPAddress();

    public static IPAddress getInstance() {
        return ourInstance;
    }

    private IPAddress() {
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public String getAcu_ip() {
        return acu_ip;
    }

    public void setAcu_ip(String acu_ip) {
        this.acu_ip = acu_ip;
    }

    public int getAcu_id() {
        return acu_id;
    }

    public void setAcu_id(int acu_id) {
        this.acu_id = acu_id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
        stream.writeObject(ipAddress);
        stream.writeObject(ip);
        stream.writeObject(port);
        stream.writeObject(acu_ip);
        stream.writeInt(acu_id);
        stream.writeInt(method);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        ipAddress = (String) stream.readObject();
        ip = (String) stream.readObject();
        port = (String) stream.readObject();
        acu_ip = (String) stream.readObject();
        acu_id = stream.readInt();
        method = stream.readInt();
    }

    public String toJSON(){
        String json = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", method);
            jsonObject.put("ip", acu_ip);
            jsonObject.put("id", acu_id);
            json = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }



}
