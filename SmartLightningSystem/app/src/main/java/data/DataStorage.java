package data;


import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import api.IPAddress;
import devices.Device;

import static android.content.Context.MODE_PRIVATE;

public class DataStorage {

    private static final String PREFERENCES_FILENAME = "SLS_preferences7";

    public void saveIpAddress(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_FILENAME, context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        if(IPAddress.getInstance().getIpAddress()!=null)
            editor.putString("address", IPAddress.getInstance().getIpAddress());
        else
            editor.putString("address", null);
        if(IPAddress.getInstance().getIp()!= null)
            editor.putString("ip", IPAddress.getInstance().getIp());
        else
            editor.putString("ip", null);
        if(IPAddress.getInstance().getPort()!=null)
            editor.putString("port", IPAddress.getInstance().getPort());
        else
            editor.putString("port", null);
        if(IPAddress.getInstance().getAcu_ip()!=null)
            editor.putString("acu_ip", IPAddress.getInstance().getAcu_ip());
        else
            editor.putString("acu_ip", null);

        editor.putInt("acu_id", IPAddress.getInstance().getAcu_id());
        editor.putInt("network_method", IPAddress.getInstance().getMethod());
        while(!editor.commit())
            editor.commit();

    }

    public void readIpAddress(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_FILENAME, MODE_PRIVATE);

        String address = sharedPref.getString("address", null);
        String ip = sharedPref.getString("ip", null);
        String port = sharedPref.getString("port", null);
        String acu_ip = sharedPref.getString("acu_ip", null);
        int acu_id = sharedPref.getInt("acu_id", -1);
        int network_method = sharedPref.getInt("network_method", 0);


        IPAddress.getInstance().setIpAddress(address);
        IPAddress.getInstance().setIp(ip);
        IPAddress.getInstance().setPort(port);
        IPAddress.getInstance().setAcu_ip(acu_ip);
        IPAddress.getInstance().setAcu_id(acu_id);
        IPAddress.getInstance().setMethod(network_method);
    }







}
