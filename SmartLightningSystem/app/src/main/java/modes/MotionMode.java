package modes;


import org.json.JSONException;
import org.json.JSONObject;

import objects.Mode;

public class MotionMode implements IMode {

    private final Mode mode = Mode.Motion;
    private int delay;
    private final int key = 1;

    public MotionMode(MotionMode motionMode) {
        this.delay = motionMode.getDelay();
    }

    public MotionMode() {
    }

    public Mode getMode() {
        return mode;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public String toJSON(int id) {

        try{
            final JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("delay", delay);
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    @Override
    public void formatJSON(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject value = jsonObject.getJSONObject("value");
            delay = value.getInt("delay");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getKey() {
        return key;
    }
}
