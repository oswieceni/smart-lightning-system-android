package modes;


import org.json.JSONException;
import org.json.JSONObject;

import objects.Mode;

public class TimeMode implements IMode {

    private final Mode mode = Mode.Time;
    private int startHour;
    private int startMinute;
    private int endHour;
    private int endMinute;
    private final int key = 2;

    public TimeMode(int startHour, int startMinute, int endHour, int endMinute) {
        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endHour = endHour;
        this.endMinute = endMinute;
    }

    public TimeMode(TimeMode timeMode) {
        this.startHour = timeMode.getStartHour();
        this.startMinute = timeMode.getStartMinute();
        this.endHour = timeMode.getEndHour();
        this.endMinute = timeMode.getEndMinute();
    }

    public TimeMode(){
    }

    public Mode getMode() {
        return mode;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }

    public String timeToString(){
        return getStartHour() + ":" + getStartMinute() + ":" + getEndHour() + ":" + getEndMinute();
    }

    public void StringToTime(String str){
        String[] parts = str.split(":");
        setStartHour(Integer.parseInt(parts[0]));
        setStartMinute(Integer.parseInt(parts[1]));
        setEndHour(Integer.parseInt(parts[2]));
        setEndMinute(Integer.parseInt(parts[3]));
    }

    @Override
    public String toJSON(int id){
        try{
            final JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("startHour", startHour);
            json.put("startMinute", startMinute);
            json.put("endHour", endHour);
            json.put("endMinute", endMinute);
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    @Override
    public void formatJSON(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject value = jsonObject.getJSONObject("value");
            startHour = value.getInt("startHour");
            startMinute = value.getInt("startMinute");
            endHour = value.getInt("endHour");
            endMinute = value.getInt("endMinute");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getKey() {
        return key;
    }
}
