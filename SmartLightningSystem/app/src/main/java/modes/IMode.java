package modes;


public interface IMode {

    int getKey();
    String toJSON(int id);
    void formatJSON(String json);

}
