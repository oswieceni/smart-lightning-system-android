package modes;


import org.json.JSONException;
import org.json.JSONObject;

import objects.ActivityProfile;
import objects.Mode;

public class ActivityMode implements IMode {

    private ActivityProfile activityProfile;
    private final Mode mode = Mode.Activity;
    private final int key = 4;

    public ActivityMode(ActivityMode activityMode) {
        this.activityProfile = activityMode.getActivityProfile();
    }

    public ActivityMode() {
    }

    public Mode getMode() {
        return mode;
    }

    public ActivityProfile getActivityProfile() {
        return activityProfile;
    }

    public void setActivityProfile(ActivityProfile activityProfile) {
        this.activityProfile = activityProfile;
    }

    @Override
    public String toJSON(int id) {

        try{
            final JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("activity", activityProfile.ordinal());
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    @Override
    public void formatJSON(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject value = jsonObject.getJSONObject("value");
            int ordinal = value.getInt("activity");

            activityProfile = ActivityProfile.values()[ordinal];

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getKey() {
        return key;
    }
}
