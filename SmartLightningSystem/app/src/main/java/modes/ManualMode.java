package modes;


import org.json.JSONException;
import org.json.JSONObject;

import objects.Color;
import objects.Mode;

public class ManualMode implements IMode {

    private final Mode mode = Mode.Manual;
    private int state = 1;
    private int brightness;
    private Color color = null;
    private final int key = 0;

    public ManualMode(ManualMode manualMode) {
        this.state = manualMode.getState();
        this.brightness = manualMode.getBrightness();
        this.color = manualMode.getColor();
    }

    public ManualMode() {
        this.state = 0 ;
        this.brightness = 50;
        this.color = Color.white;
    }

    public Mode getMode() {
        return mode;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toJSON(int id) {

        try{
            final JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("state", state);
            json.put("brightness", brightness);
            json.put("color", color.getColorValue());
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    @Override
    public void formatJSON(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject value = jsonObject.getJSONObject("value");
            state = value.getInt("state");
            brightness = value.getInt("brightness");
            int ordinal = value.getInt("color");
            color = Color.values()[ordinal];
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getKey() {
        return key;
    }
}
