package modes;


import org.json.JSONException;
import org.json.JSONObject;

import objects.Mode;

public class PartyMode implements IMode {

    private final Mode mode = Mode.Party;
    private int speed = 0;
    private final int key = 5;


    public PartyMode(PartyMode partyMode) {
        this.speed = partyMode.getSpeed();
    }

    public PartyMode() {
    }

    public Mode getMode() {
        return mode;
    }


    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toJSON(int id) {

        try{
            final JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("speed", speed);
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    @Override
    public void formatJSON(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject value = jsonObject.getJSONObject("value");
            speed = value.getInt("speed");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getKey() {
        return key;
    }
}
