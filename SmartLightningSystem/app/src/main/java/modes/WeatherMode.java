package modes;


import org.json.JSONException;
import org.json.JSONObject;

import objects.Mode;

public class WeatherMode implements IMode {

    private final Mode mode = Mode.Weather;
    private String cloudImg;
    private int cloudPercentage;
    private String cloudDescription;
    private int temperature;
    private String localisation;
    private boolean day;
    private String color;
    private final int key = 6;

    public WeatherMode() {}

    public Mode getMode() {
        return mode;
    }

    public String getCloudImg() {
        return cloudImg;
    }

    public void setCloudImg(String cloudImg) {
        this.cloudImg = cloudImg;
    }

    public int getCloudPercentage() {
        return cloudPercentage;
    }

    public void setCloudPercentage(int cloudPrecentage) {
        this.cloudPercentage = cloudPrecentage;
    }

    public String getCloudDescription() {
        return cloudDescription;
    }

    public void setCloudDescription(String cloudDescription) {
        this.cloudDescription = cloudDescription;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public boolean isDay() {
        return day;
    }

    public void setDay(boolean day) {
        this.day = day;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int getKey() {
        return key;
    }

    @Override
    public String toJSON(int id) {
        return null;
    }

    @Override
    public void formatJSON(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject condition = jsonObject.getJSONObject("condition");
            cloudImg = condition.getString("icon");
            cloudPercentage = jsonObject.getInt("cloudCoverage");
            temperature = jsonObject.getInt("temperature");
            localisation = jsonObject.getString("location");
            day = jsonObject.getBoolean("isDay");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
