package activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import api.APIConnection;
import devices.Device;
import devices.DevicesList;
import objects.Color;
import com.json.sls.smartlightningsystem.CustomGrid;

import objects.Mode;
import com.json.sls.smartlightningsystem.R;

public class ManualModeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener{

    GridView grid;
    Color pickedColor = Color.white;
    Color[] colors;

    SeekBar brightnessSeekBar;
    TextView progressTxtView;

    int progressValue = 1;

    ViewGroup devices_view;

    private GestureDetectorCompat mDetector;

    Device device;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manual_mode);

        final DevicesList devicesList = DevicesList.getInstance();
        device = devicesList.getCurrentDevice();
        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);
        grid = (GridView) findViewById(R.id.grid_color);


        progressTxtView = (TextView) findViewById(R.id.brightness_progress_txt_view);
        brightnessSeekBar = (SeekBar) findViewById(R.id.brightness_seek_bar);
        brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if(progressValue>0){
                    progressTxtView.setText(progressValue+ "%");

                    if(device!=null){
                        device.getManualMode().setBrightness(progressValue);
                        device.setState(1);
                    }
                }
            }
        });

        colors = Color.values();
        int[] colorsId = new int[colors.length];
        String[] colorsName = new String[colors.length];

        int i = 0;
        for (Color color : colors) {
            colorsName[i] = color.toString();
            colorsId[i++] = color.getColorImgId();
        }


        final CustomGrid adapter = new CustomGrid(ManualModeActivity.this, colorsName, colorsId);

        if(device!=null){
            if(device.getManualMode().getColor()!=null){
                pickedColor = device.getManualMode().getColor();
                int position = pickedColor.ordinal();
                adapter.setCheckedBulb(position);

            }


            if(device.getManualMode().getBrightness()!=0){
                brightnessSeekBar.setProgress(device.getManualMode().getBrightness());
            }
        }

        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                for(int i=0; i<grid.getChildCount(); i++)
                    grid.getChildAt(i).setActivated(false);
                view.setActivated(true);
                Color color = colors[position];
                pickedColor = color;
                if(device != null){
                    device.setState(1);
                    device.getManualMode().setColor(color);

                }

                setValuesToAPI();


            }
        });

    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        grid.performClick();
    }

    private void setValuesToAPI() {

        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPostExecute(String txt) {
                super.onPostExecute(txt);

                Context context = getApplicationContext();
                CharSequence text = txt;
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                if(device!= null){
                    device.setMode(Mode.Manual);
                    String answer = new APIConnection().setMode(device);
                    String response = new APIConnection().setManualMode(device);
                    return response + "\n" + answer;
                }

                return "Choose device!";
            }
        }.execute();

    }


    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
