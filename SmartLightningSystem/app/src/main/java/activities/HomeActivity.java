package activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.GestureDetectorCompat;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.json.sls.smartlightningsystem.R;

import java.util.ArrayList;
import java.util.List;

import api.APIConnection;
import data.DataStorage;
import devices.Device;
import devices.DevicesList;

public class HomeActivity extends Activity  implements GestureDetector.OnGestureListener{
    ImageButton settings;
    ImageButton light_settings;
    ImageButton profiles;
    ViewGroup devices_view;

    List<Device> devices;

    private GestureDetectorCompat mDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);

        settings = (ImageButton) findViewById(R.id.settings);
        light_settings = (ImageButton) findViewById(R.id.light_settings);
        profiles = (ImageButton) findViewById(R.id.profiles);

        devices = new ArrayList<>();
        getLastData();
        getListOfDevices();


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });
        light_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                startActivity(intent);

            }
        });

        profiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfilesActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getLastData() {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                new DataStorage().readIpAddress(getApplicationContext());


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);


            }
        }.execute();

    }


    public void getListOfDevices(){

        new AsyncTask<Object, Object, String>(){

            @Override
            protected String doInBackground(Object... params) {
                boolean isConnection = new APIConnection().checkConnection(getApplicationContext());
                if(isConnection){
                    String json = new APIConnection().getDevicesList();
                    if(json != null){
                        List<Device> workList = DevicesList.getInstance().formatJSON(json);

                        for(Device device: workList){
                            String data = new APIConnection().getDeviceData(device.getId());
                            if(data!=null)
                                device.setModeData(data);
                            devices.add(new Device(device));
                        }
                        if(DevicesList.getInstance().getCurrentDevice()==null){
                            devices.get(0).setChecked(true);
                            DevicesList.getInstance().setCurrentDevice(devices.get(0));
                        }
                        if(DevicesList.getInstance().getDevices()!=null)
                            DevicesList.getInstance().getDevices().clear();
                        DevicesList.getInstance().setDevices(devices);


                    }else{
                        return  "No connection to server.\nCheck IP configuration";
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                if(string!=null){
                    Context context = getApplicationContext();
                    CharSequence text = string;
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        }.execute();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onDestroy() {
        android.os.Process.killProcess(android.os.Process.myPid());
        super.onDestroy();
    }

}
