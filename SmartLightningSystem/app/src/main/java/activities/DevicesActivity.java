package activities;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.Toast;

import api.APIConnection;
import devices.Device;
import devices.DevicesAdapter;
import devices.DevicesList;

import com.json.sls.smartlightningsystem.R;

import java.util.ArrayList;
import java.util.List;

public class DevicesActivity extends Activity implements GestureDetector.OnGestureListener{
    RecyclerView recyclerView;
    DevicesAdapter devicesAdapter;
    List<Device> devices = new ArrayList<>();
    private GestureDetectorCompat mDetector;
    Device currentDevice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mDetector = new GestureDetectorCompat(this,this);
        setContentView(R.layout.devices);
        updateDevices();
        getListOfDevices();
        devicesAdapter = new DevicesAdapter(devices);

        recyclerView = (RecyclerView) findViewById(R.id.device_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(devicesAdapter);



    }


    public void updateDevices(){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                List<Device> deviceList;
                currentDevice = DevicesList.getInstance().getCurrentDevice();
                if(DevicesList.getInstance().getDevices().size()>0){
                    deviceList = DevicesList.getInstance().getDevices();
                    for(Device device: deviceList){
                        String data = new APIConnection().getDeviceData(device.getId());
                        if(data!=null)
                            device.setModeData(data);
                        addToList(device);
                    }
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                devicesAdapter.notifyDataSetChanged();
            }
        }.execute();
    }


    public void addToList(Device newDevice){
        if(newDevice.getId()==currentDevice.getId())
            newDevice.setChecked(true);
        if(devices.size()>0){
            for(Device device: devices){
                if(device.getId() == newDevice.getId())
                    device.setData(newDevice);
            }
        }
    }

    public void getListOfDevices(){

        new AsyncTask<Object, Object, String>(){

            @Override
            protected String doInBackground(Object... params) {
                if(devices.size()>0) return null;
                boolean isConnection = new APIConnection().checkConnection(getApplicationContext());
                if(isConnection){
                    String json = new APIConnection().getDevicesList();
                    if(json != null){
                        List<Device> workList = DevicesList.getInstance().formatJSON(json);
                        if(DevicesList.getInstance().getDevices().isEmpty()){
                            return "No devices found";
                        }else{
                            for(Device device: workList){
                                String data = new APIConnection().getDeviceData(device.getId());
                                if(data!=null)
                                    device.setModeData(data);
                                devices.add(new Device(device));
                            }
                            if(DevicesList.getInstance().getCurrentDevice()==null){
                                devices.get(0).setChecked(true);
                                DevicesList.getInstance().setCurrentDevice(devices.get(0));
                            }
                            if(DevicesList.getInstance().getDevices()!=null)
                                DevicesList.getInstance().getDevices().clear();
                            DevicesList.getInstance().setDevices(devices);
                        }

                    }else{
                        return  "No connection to server.\nCheck IP configuration";
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                if(string!=null){
                    Context context = getApplicationContext();
                    CharSequence text = string;
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                }

                devicesAdapter.notifyDataSetChanged();
            }
        }.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        List<Device> finalList = devicesAdapter.getDevices();
        for(Device device: finalList){
            changeName(device);
        }
        DevicesList.getInstance().setDevices(finalList);

    }

    private void changeName(final  Device device){
        new AsyncTask<Device, Void, String>(){

            @Override
            protected String doInBackground(Device... params) {
                new APIConnection().setDeviceName(params[0]);
                return null;
            }
        }.execute(device);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 50;
    private static final int SWIPE_VELOCITY_THRESHOLD = 50;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view

                    } else {
                        //on swipe right
                        finish();
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
