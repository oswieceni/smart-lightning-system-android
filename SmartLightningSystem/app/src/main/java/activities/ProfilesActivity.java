package activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.json.sls.smartlightningsystem.CustomGrid;
import com.json.sls.smartlightningsystem.R;

import objects.Mode;

public class ProfilesActivity extends AppCompatActivity implements GestureDetector.OnGestureListener{

    GridView grid;
    ViewGroup devices_view;

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profiles);
        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);
        final Mode[] modes = Mode.values();
        final String[] descriptions = new String[modes.length];
        int[] imagesId = new int[modes.length];
        int i = 0;
        for(Mode mode: modes){
            descriptions[i] = mode.toString() + " Mode";
            imagesId[i++] = mode.getImageId();
        }
        CustomGrid adapter = new CustomGrid(ProfilesActivity.this, descriptions, imagesId);
        grid=(GridView)findViewById(R.id.grid_settings);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent;
                Mode mode = modes[position];
                switch (mode){
                    case Manual:
                        intent = new Intent(getApplicationContext(), ManualModeActivity.class);
                        startActivity(intent);
                        break;
                    case Motion:
                        intent = new Intent(getApplicationContext(), MotionModeActivity.class);
                        startActivity(intent);
                        break;
                    case Time:
                        intent = new Intent(getApplicationContext(), TimeModeActivity.class);
                        startActivity(intent);
                        break;
                    case Weather:
                        intent = new Intent(getApplicationContext(), WeatherModeActivity.class);
                        startActivity(intent);
                        break;
                    case Activity:
                        intent = new Intent(getApplicationContext(), IntensityModeActivity.class);
                        startActivity(intent);
                        break;
                    case Party:
                        intent = new Intent(getApplicationContext(), PartyModeActivity.class);
                        startActivity(intent);
                        break;
                    case Gesture:
                        intent = new Intent(getApplicationContext(), GestureModeActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
