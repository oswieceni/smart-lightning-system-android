package activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import api.APIConnection;
import devices.Device;
import devices.DevicesList;
import objects.ActivityProfile;
import objects.Mode;
import com.json.sls.smartlightningsystem.R;


public class IntensityModeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {


    Button activateBtn;
    Button computerBtn;
    Button bookBtn;
    Button tvBtn;
    ProgressBar progressBar;
    ActivityProfile pickedProfile = null;
    ViewGroup devices_view;

    private GestureDetectorCompat mDetector;
    Device device;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intensity_mode);

        bookBtn = (Button) findViewById(R.id.book);
        progressBar = (ProgressBar) findViewById(R.id.intensity_mode_progress_bar);
        computerBtn = (Button) findViewById(R.id.computer);
        tvBtn = (Button) findViewById(R.id.tv);
        activateBtn = (Button) findViewById(R.id.activate);



        DevicesList devicesList = DevicesList.getInstance();
        device = devicesList.getCurrentDevice();
        updateDevicesData();

        if(device!=null){
            if(device.getActivityMode().getActivityProfile()!=null){
                switch(device.getActivityMode().getActivityProfile()){
                    case Book:
                        pickedProfile = ActivityProfile.Book;
                        bookBtn.setActivated(true);
                        break;
                    case Computer:
                        pickedProfile = ActivityProfile.Computer;
                        computerBtn.setActivated(true);
                        break;
                    case Tv:
                        pickedProfile = ActivityProfile.Tv;
                        tvBtn.setActivated(true);
                        break;
                }
            }
        }


        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);

        progressBar.setVisibility(View.INVISIBLE);
        computerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(computerBtn.isActivated()){
                    computerBtn.setActivated(false);
                    pickedProfile = null;
                }else{
                    bookBtn.setActivated(false);
                    tvBtn.setActivated(false);
                    computerBtn.setActivated(true);
                    pickedProfile = ActivityProfile.Computer;
                }

            }
        });
        bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bookBtn.isActivated()){
                    bookBtn.setActivated(false);
                    pickedProfile = null;
                }else{
                    bookBtn.setActivated(true);
                    tvBtn.setActivated(false);
                    computerBtn.setActivated(false);
                    pickedProfile = ActivityProfile.Book;
                }
            }
        });
        tvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvBtn.isActivated()){
                    tvBtn.setActivated(false);
                    pickedProfile = null;
                }else{
                    bookBtn.setActivated(false);
                    tvBtn.setActivated(true);
                    computerBtn.setActivated(false);
                    pickedProfile = ActivityProfile.Tv;
                }
            }
        });

        activateBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                new AsyncTask<Void, Void, String>(){
                    //wysłanie zapytania o zmianę profilu i odebranie odpowiedzi
                    @Override
                    protected String doInBackground(Void... params) {
                        String answer;
                        if(device!=null){
                            device.getActivityMode().setActivityProfile(pickedProfile);
                            device.setMode(Mode.Activity);
                            answer = new APIConnection().setMode(device);
                            new APIConnection().setActivityMode(device);

                        }else
                            answer = "Missing device";

                        return answer;
                    }

                    //wyświetlenie odpowiedzi serwera i zakończenie aktywności
                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        CharSequence text = s;
                        Context context = getApplicationContext();
                        if(pickedProfile==null)
                            text = "Choose profile";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        finish();
                    }
                }.execute();
            }
        });

    }

    private void updateDevicesData() {

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                if(device!=null){
                    String data = new APIConnection().getDeviceData(device.getId());
                    if(data!=null)
                        DevicesList.getInstance().getDeviceById(device.getId()).setModeData(data);
                }

                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }

}
