package activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import api.APIConnection;
import devices.Device;
import devices.DevicesList;
import objects.Mode;
import com.json.sls.smartlightningsystem.R;


public class PartyModeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener{

    Button activateBtn;
    TextView animationProgressTxtView;
    SeekBar animationSeekBar;

    ProgressBar progressBar;

    int progressValue = 0;
    int speed = 0;

    ViewGroup devices_view;

    private GestureDetectorCompat mDetector;
    Device device;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.party_mode);

        DevicesList devicesList = DevicesList.getInstance();
        device = devicesList.getCurrentDevice();

        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);
        progressBar = (ProgressBar) findViewById(R.id.party_mode_progress_bar);
        progressBar.setVisibility(View.INVISIBLE);
        activateBtn = (Button) findViewById(R.id.activate);
        animationProgressTxtView = (TextView) findViewById(R.id.animation_progress_txt_view);
        animationSeekBar = (SeekBar) findViewById(R.id.animation_seek_bar);
        activateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                new AsyncTask<Void, Void, String>(){
                    //wysłanie zapytania o zmianę profilu i odebranie odpowiedzi
                    @Override
                    protected String doInBackground(Void... params) {
                        String answer, response;
                        if(device!=null){
                            device.setMode(Mode.Party);
                            answer = new APIConnection().setMode(device);
                            device.getPartyMode().setSpeed(speed);
                            response = new APIConnection().setPartyMode(device);
                        }else
                            return "Choose device!";

                        return answer + "\n" + response;
                    }

                    //wyświetlenie odpowiedzi serwera i zakończenie aktywności
                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        Context context = getApplicationContext();
                        CharSequence text = s;
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        finish();
                    }
                }.execute();
            }
        });

        if(device!=null)
            animationSeekBar.setProgress((device.getPartyMode().getSpeed()+1)*10);

        animationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = progress;
                if(progressValue >= 1 && progressValue <= 10){
                    speed = 0;
                } else if(progressValue > 10 && progressValue <= 20){
                    speed = 1;
                }else if(progressValue > 20 && progressValue <= 30){
                    speed = 2;
                }else if(progressValue >30 && progressValue <= 40){
                    speed = 3;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                display(speed);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                display(speed);
            }
        });


    }
    public void display(int value){


        switch (value){
            case 0:
                animationProgressTxtView.setText("slow");
                break;
            case 1:
                animationProgressTxtView.setText("normal");
                break;
            case 2:
                animationProgressTxtView.setText("fast");
                break;
            case 3:
                animationProgressTxtView.setText("insane");
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
