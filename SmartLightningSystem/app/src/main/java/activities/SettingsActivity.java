package activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.json.sls.smartlightningsystem.R;

import java.util.ArrayList;
import java.util.List;

import api.APIConnection;
import api.IPAddress;
import data.DataStorage;

public class SettingsActivity extends Activity implements GestureDetector.OnGestureListener {

    ViewGroup devices_view;
    EditText ip_edit_txt;
    Button ok_button;
    EditText port_edit_txt;
    EditText ip_acu_edit_txt;
    EditText id_acu_edit_txt;
    Spinner network_method_spinner;


    String port = null;
    String ip = null;
    String address = null;
    String ip_acu = null;
    String id_acu = null;
    int network_choice = 0;

    ArrayAdapter<String> dataAdapter;

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);
        ip_edit_txt = (EditText) findViewById(R.id.ip_query);
        port_edit_txt = (EditText) findViewById(R.id.port_query);
        ok_button = (Button) findViewById(R.id.activate);
        ip_acu_edit_txt = (EditText)findViewById(R.id.ip_acu_query);
        id_acu_edit_txt = (EditText) findViewById(R.id.id_acu_query);
        network_method_spinner = (Spinner) findViewById(R.id.network_spinner);

        network_method_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                network_choice = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> networks = new ArrayList<>();
        networks.add("local");
        networks.add("global");

        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, networks);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        network_method_spinner.setAdapter(dataAdapter);

        readData();

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                port = port_edit_txt.getText().toString();
                ip = ip_edit_txt.getText().toString();
                ip_acu = ip_acu_edit_txt.getText().toString();
                id_acu = id_acu_edit_txt.getText().toString();

                if(!ip.isEmpty() && !port.isEmpty()){
                    address = "http://" + ip + ":" + port;
                    IPAddress.getInstance().setIpAddress(address);

                }
                if(!ip.isEmpty())
                    IPAddress.getInstance().setIp(ip);
                if(!port.isEmpty())
                IPAddress.getInstance().setPort(port);
                if(!ip_acu.isEmpty())
                IPAddress.getInstance().setAcu_ip(ip_acu);
                if(!id_acu.isEmpty()){
                    int id_acu_int = Integer.parseInt(id_acu);
                    IPAddress.getInstance().setAcu_id(id_acu_int);
                }
                IPAddress.getInstance().setMethod(network_choice);


                new AsyncTask<Void, Void, String>(){
                    @Override
                    protected String doInBackground(Void... params) {
                        new DataStorage().saveIpAddress(getApplicationContext());
                        boolean success = new APIConnection().checkConnection(getApplicationContext());
                        String text;
                        if(success){
                            text =  "Connected to server";
                            if(ip_acu != null && id_acu!= null);
                                new APIConnection().setAcuAdress();

                        }
                        else
                            text =  "Connection failed";
                        return text;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        CharSequence text = s;
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        finish();
                    }
                }.execute();


            }
        });
    }

    private void readData() {

        if(IPAddress.getInstance().getIp()!= null)
            ip_edit_txt.setText(IPAddress.getInstance().getIp());
        if(IPAddress.getInstance().getPort()!=null)
            port_edit_txt.setText(IPAddress.getInstance().getPort());
        if(IPAddress.getInstance().getAcu_ip() != null)
            ip_acu_edit_txt.setText(IPAddress.getInstance().getAcu_ip());
        if(IPAddress.getInstance().getAcu_id() != -1)
            id_acu_edit_txt.setText(String.valueOf(IPAddress.getInstance().getAcu_id()));
        if(IPAddress.getInstance().getMethod() != -1){
            network_method_spinner.setSelection(IPAddress.getInstance().getMethod());
            dataAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
