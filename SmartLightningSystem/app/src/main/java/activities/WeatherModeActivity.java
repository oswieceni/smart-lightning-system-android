package activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import api.APIConnection;
import devices.Device;
import devices.DevicesList;
import modes.WeatherMode;
import objects.Mode;
import com.json.sls.smartlightningsystem.R;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherModeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener{

        Button activateBtn;
        ImageView weatherImgBtn;
        TextView localisationTxtView;
        TextView temperatureTxtView;
        TextView cloudPercentageTxtView;
        ProgressBar progressBar;
        LinearLayout weatherLayout;
        ViewGroup devices_view;

        private GestureDetectorCompat mDetector;

        Device device;

        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.weather_mode);
            DevicesList devicesList = DevicesList.getInstance();
            device = devicesList.getCurrentDevice();
            mDetector = new GestureDetectorCompat(this, this);
            devices_view = (ViewGroup) findViewById(R.id.devices_view);
            progressBar = (ProgressBar) findViewById(R.id.weather_mode_progress_bar);
            progressBar.setVisibility(View.INVISIBLE);
            weatherLayout = (LinearLayout) findViewById(R.id.frame_layout_weathercast);
            activateBtn = (Button) findViewById(R.id.activate);
            weatherImgBtn = (ImageView) findViewById(R.id.cloudy_image);
            localisationTxtView = (TextView) findViewById(R.id.localisation_text_view);
            temperatureTxtView = (TextView) findViewById(R.id.temperature_text_view);
            cloudPercentageTxtView = (TextView) findViewById(R.id.cloudy_percentage_text_view);

            getCurrentWeathercast();

            weatherLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCurrentWeathercast();
                }
            });

            activateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    new AsyncTask<Void, Void, String>() {
                        //wysłanie zapytania o zmianę profilu i odebranie odpowiedzi
                        @Override
                        protected String doInBackground(Void... params) {
                            String answer;
                            if(device!=null){
                                device.setMode(Mode.Weather);
                                answer = new APIConnection().setMode(device);
                            }else
                                answer = "Missing device";

                            return answer;
                        }

                        //wyświetlenie odpowiedzi serwera i zakończenie aktywności
                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            Context context = getApplicationContext();
                            CharSequence text;
                            if(device!=null)
                                text = "Device id:" + device.getId() +"\n" + s;
                            else
                                text =  s;
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            finish();
                        }
                    }.execute();
                }
            });

        }


    private  void getCurrentWeathercast(){
        progressBar.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Void, WeatherMode>(){

            @Override
            protected WeatherMode doInBackground(Void... params) {
                boolean isConnection = new APIConnection().checkConnection(getApplicationContext());
                if(isConnection){
                    String json = new APIConnection().getWeatherMode();
                    if(json!=null){
                        WeatherMode weather = new WeatherMode();
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            JSONObject condition = jsonObject.getJSONObject("condition");
                            weather.setCloudImg(condition.getString("icon"));
                            weather.setCloudPercentage(jsonObject.getInt("cloudCoverage"));
                            weather.setTemperature(jsonObject.getInt("temperature"));
                            weather.setLocalisation(jsonObject.getString("location"));
                            weather.setDay(jsonObject.getBoolean("isDay"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return weather;
                    }
                }


                return null;

            }

            @Override
            protected void onPostExecute(WeatherMode weather) {
                super.onPostExecute(weather);
                if (weather != null) {
                    setWeathercast(weather.isDay(), weather.getCloudImg(), weather.getLocalisation(), weather.getTemperature(), weather.getCloudPercentage());
                    progressBar.setVisibility(View.INVISIBLE);
                }else{
                    Context context = getApplicationContext();
                    CharSequence text = "No connection to server.\nCheck IP configuration and try again";
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                }


            }
        }.execute();

    }
    private void setWeathercast(boolean day, String imageName, String localisation, int temperature, int cloudPercentage)  {

        if (day) {
            weatherImgBtn.setBackgroundResource(getResources().getIdentifier("day" + imageName, "drawable", getPackageName()));

        } else
            weatherImgBtn.setBackgroundResource(getResources().getIdentifier("night" + imageName, "drawable", getPackageName()));
        localisationTxtView.setText(localisation);
        temperatureTxtView.setText(temperature + "\u00b0" + "C");
        cloudPercentageTxtView.setText(cloudPercentage + "% Cloudy");

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
