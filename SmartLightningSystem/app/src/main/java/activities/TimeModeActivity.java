package activities;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import api.APIConnection;
import devices.Device;

import com.json.sls.smartlightningsystem.R;

import devices.DevicesList;
import modes.TimeMode;

import java.text.ParseException;
import java.util.Date;

import objects.Mode;


public class TimeModeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener{

    Button activateBtn;
    Button setTimeOnbtn;
    Button setTimeOffbtn;

    TextView setTimeOntxt;
    TextView setTimeOfftxt;
    ProgressBar progressBar;

    ViewGroup devices_view;

    private GestureDetectorCompat mDetector;

    static final int DIALOG_ID =0;

    int pickedHour;
    int pickedMinute;

    int startHour;
    int startMinute;
    int endHour;
    int endMinute;

    boolean timeOn = true;

    Device device;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.time_mode);
        DevicesList devicesList = DevicesList.getInstance();
        device = devicesList.getCurrentDevice();
        mDetector = new GestureDetectorCompat(this,this);
        devices_view = (ViewGroup) findViewById(R.id.devices_view);
        setTimeOntxt = (TextView) findViewById(R.id.timeOntxt);
        setTimeOfftxt = (TextView) findViewById(R.id.timeOfftxt);
        progressBar = (ProgressBar) findViewById(R.id.time_mode_progress_bar);
        progressBar.setVisibility(View.INVISIBLE);


        if(device!=null){
            displayTime(device.getTime());
            timeOn = false;
            displayTime(device.getTime());
        }


        showTimeONPickerDialog();
        showTimeOFFPickerDialog();

        activateBtn = (Button) findViewById(R.id.activate);
        activateBtn.setEnabled(true);
        activateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                new AsyncTask<Void, Void, String>(){
                    //wysłanie zapytania o zmianę profilu i odebranie odpowiedzi
                    @Override
                    protected String doInBackground(Void... params) {
                        if(device!=null){
                            TimeMode time = new TimeMode(startHour, startMinute, endHour, endMinute);
                            device.setTime(time);
                            device.setMode(Mode.Time);
                            String answer = new APIConnection().setMode(device);
                            String response = new APIConnection().setTimeMode(device);
                            return answer + "\n" + response;

                        }else
                            return "Choose device";

                    }
                    //wyświetlenie odpowiedzi serwera i zakończenie aktywności
                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        Context context = getApplicationContext();
                        CharSequence text = s;
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        finish();
                    }
                }.execute();
            }
        });
    }

    //wybranie godziny włączenia oświetlenia
    public void showTimeONPickerDialog(){
        setTimeOnbtn = (Button) findViewById(R.id.timeOn);

        setTimeOnbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeOn = true;
                showDialog(DIALOG_ID);
            }
        });
    }

    //wybranie godziny wyłączenia oświetlenia
    public void showTimeOFFPickerDialog(){
        setTimeOffbtn = (Button) findViewById(R.id.timeOff);

        setTimeOffbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeOn = false;
                showDialog(DIALOG_ID);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if(id==DIALOG_ID){
            return new TimePickerDialog(TimeModeActivity.this, kTimePickerListener, pickedHour, pickedMinute, true);
        }
        return null;
    }

    //akcja - wybór godziny
    protected TimePickerDialog.OnTimeSetListener kTimePickerListener = new TimePickerDialog.OnTimeSetListener(){

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            TimeMode time  = new TimeMode();
            if(timeOn){
                startHour = hourOfDay;
                startMinute = minute;
                time.setStartHour(startHour);
                time.setStartMinute(startMinute);
                displayTime(time);


            }else{
                endHour = hourOfDay;
                endMinute = minute;

                time.setEndHour(endHour);
                time.setEndMinute(endMinute);
                displayTime(time);
            }

            activateBtn.setEnabled(true);

        }
    };

    //wyświetlenie wybranych godzin
    private void displayTime(TimeMode time) {
        java.text.SimpleDateFormat timeFormat = new java.text.SimpleDateFormat("HH:mm");
        if(timeOn){
            setTimeOntxt.clearComposingText();
            String startHourStr = Integer.toString(time.getStartHour());
            String startMinuteStr = Integer.toString(time.getStartMinute());
            String hour = startHourStr + ":" + startMinuteStr;
            setTimeOntxt.setText(hour);
            Date date;
            try {
                date = timeFormat.parse(hour);
                setTimeOntxt.setText(timeFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            setTimeOfftxt.clearComposingText();
            String endHourStr = Integer.toString(time.getEndHour());
            String endMinuteStr = Integer.toString(time.getEndMinute());
            String hour = endHourStr + ":" + endMinuteStr;
            Date date;
            try {
                date = timeFormat.parse(hour);
                setTimeOfftxt.setText(timeFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        //on swipe right show devices view
                        Intent intent = new Intent(getApplicationContext(), DevicesActivity.class);
                        startActivity(intent);
                        this.overridePendingTransition(R.anim.slide_in_left,
                                R.anim.slide_out_left);

                    } else {
                        //on swipe right
                    }
                }
            } else {
                if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        //on swipe down
                    } else {
                        //on swipe up
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return false;
    }
}
