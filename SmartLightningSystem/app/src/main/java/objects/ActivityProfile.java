package objects;


import com.json.sls.smartlightningsystem.R;

public enum ActivityProfile {
    Computer (R.drawable.laptop72px),
    Tv (R.drawable.tv72px),
    Book (R.drawable.literature72px);


    private final int imageId;
    ActivityProfile(int imageId) {
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }
}
