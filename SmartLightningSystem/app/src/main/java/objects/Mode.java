package objects;

import com.json.sls.smartlightningsystem.R;

public enum Mode {
    Manual (R.drawable.manual72px),
    Motion (R.drawable.parkour72px),
    Time (R.drawable.bigben72px),
    Weather (R.drawable.sun72px),
    Activity (R.drawable.blind_automatic_mode72px),
    Party (R.drawable.dancingpartyfilled72px),
    Gesture (R.drawable.startrekgesture72px);

    private final int imageId;
    Mode(int imageId) {
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }
}
