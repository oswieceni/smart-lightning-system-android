package objects;


import com.json.sls.smartlightningsystem.R;

public enum Color {

    red (15601680, R.color.red, R.drawable.idea_red48px),
    green (3655513, R.color.green, R.drawable.idea_green48px),
    blue (281525, R.color.blue, R.drawable.idea_blue48px),

    red_orange (15756858, R.color.red_orange, R.drawable.idea_red_orange48px) ,
    green_spring (8122776, R.color.green_spring, R.drawable.idea_green_spring48px),
    blue_violet (5469134, R.color.blue_violet, R.drawable.idea_blue_violet48px),

    orange (13792345, R.color.orange, R.drawable.idea_orange48px),
    green_cyan (8630414, R.color.green_cyan, R.drawable.idea_green_cyan48px),
    violet (8468881, R.color.violet, R.drawable.idea_violet48px),

    orange_yellow (16163907, R.color.orange_yellow, R.drawable.idea_orange_yellow48px),
    cyan (6414792, R.color.cyan, R.drawable.idea_cyan48px),
    violet_pink (14760823, R.color.violet, R.drawable.idea_violet_pink48px),

    yellow (15455338, R.color.yellow, R.drawable.idea_yellow48px),
    cyan_blue (2598616, R.color.cyan_blue, R.drawable.idea_cyan_blue48px),
    pink (15030187, R.color.pink, R.drawable.idea_pink48px),
    white(16777215, R.color.white, R.drawable.idea_white48px);





    private final int colorId;
    private final int colorImgId;
    private final int colorValue;

    Color(int colorValue, int colorId, int colorImgId) {
        this.colorValue = colorValue;
        this.colorId = colorId;
        this.colorImgId = colorImgId;
    }

    public int getColorId() {
        return colorId;
    }

    public int getColorImgId() {
        return colorImgId;
    }

    public int getColorValue() {
        return colorValue;
    }


}
