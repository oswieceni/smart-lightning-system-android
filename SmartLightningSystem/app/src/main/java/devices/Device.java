package devices;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import modes.ActivityMode;
import modes.IMode;
import modes.ManualMode;
import modes.MotionMode;
import modes.PartyMode;
import modes.WeatherMode;
import objects.Color;
import objects.Mode;
import modes.TimeMode;

public class Device {

    private int id;
    private String name;
    private int state;
    private Mode mode;
    private ManualMode manualMode;
    private ActivityMode activityMode;
    private MotionMode motionMode;
    private PartyMode partyMode;
    private TimeMode time;
    private boolean checked;
    private int type;
    private List<IMode> modes = null;

    public Device(Device device){
        this.id = device.getId();
        this.name = device.getName();
        this.state = device.getState();
        this.mode = device.getMode();
        this.manualMode = new ManualMode(device.getManualMode());
        this.activityMode = new ActivityMode(device.getActivityMode());
        this.partyMode = new PartyMode(device.getPartyMode());
        this.motionMode = new MotionMode(device.getMotionMode());
        this.time = new TimeMode(device.getTime());
        this.checked = device.isChecked();
        this.type = device.getType();

        modes = new ArrayList<>();


        modes.add(manualMode);
        modes.add(motionMode);
        modes.add(time);
        modes.add(activityMode);
        modes.add(partyMode);
    }

    public Device() {
        manualMode = new ManualMode();
        activityMode = new ActivityMode();
        motionMode = new MotionMode();
        partyMode = new PartyMode();
        time = new TimeMode();
        modes = new ArrayList<>();


        modes.add(manualMode);
        modes.add(motionMode);
        modes.add(time);
        modes.add(activityMode);
        modes.add(partyMode);

    }

    public void setData(Device device){
        this.id = device.getId();
        this.name = device.getName();
        this.state = device.getState();
        this.mode = device.getMode();
        this.manualMode = new ManualMode(device.getManualMode());
        this.activityMode = new ActivityMode(device.getActivityMode());
        this.partyMode = new PartyMode(device.getPartyMode());
        this.motionMode = new MotionMode(device.getMotionMode());
        this.time = new TimeMode(device.getTime());
        this.type = device.getType();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ManualMode getManualMode() {
        return manualMode;
    }

    public void setManualMode(ManualMode manualMode) {
        this.manualMode = new ManualMode(manualMode);
    }

    public ActivityMode getActivityMode() {
        return activityMode;
    }

    public void setActivityMode(ActivityMode activityMode) {
        this.activityMode = new ActivityMode(activityMode);
    }

    public MotionMode getMotionMode() {
        return motionMode;
    }

    public void setMotionMode(MotionMode motionMode) {
        this.motionMode = new MotionMode(motionMode);
    }

    public PartyMode getPartyMode() {
        return partyMode;
    }

    public void setPartyMode(PartyMode partyMode) {
        this.partyMode = new PartyMode(partyMode);
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public TimeMode getTime() {
        return time;
    }

    public void setTime(TimeMode time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public String modeToJSON(){
        try{
            JSONObject profil = new JSONObject();
            profil.put("id", id);
            profil.put("mode", mode.ordinal());
            return profil.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    public void setModeData(String json){
        try{
            JSONObject devicesData = new JSONObject(json);
            JSONArray settings = devicesData.getJSONArray("settings");

            for(int i=0; i<settings.length(); i++){
                JSONObject mode = settings.getJSONObject(i);
                int key = mode.getInt("key");
                String data = mode.getJSONObject("value").toString();
                if(modes!=null)
                for(IMode imode: modes){
                    if(imode.getKey()==key){
                        imode.formatJSON(data);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String nameToJSON(){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id);
            jsonObject.put("name", name);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }


}
