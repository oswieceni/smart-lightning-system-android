package devices;


import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.json.sls.smartlightningsystem.R;

import java.util.ArrayList;
import java.util.List;

import api.APIConnection;
import objects.Mode;


public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.MyViewHolder> {

    private List<Device> devices;
    private List<CheckBox> checkBoxList = new ArrayList<>();


    public List<Device> getDevices(){return devices;}

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mode, state;
        public EditText name;
        public Button turnOnOff;
        public CheckBox choosedDevice;


        public MyViewHolder(View view) {
            super(view);

            mode = (TextView) view.findViewById(R.id.device_mode);
            state = (TextView) view.findViewById(R.id.device_state);
            name = (EditText) view.findViewById(R.id.device_name);
            turnOnOff = (Button) view.findViewById(R.id.turnOnOff);
            choosedDevice = (CheckBox) view.findViewById(R.id.device_check_box);
            checkBoxList.add(choosedDevice);
        }
    }

    public DevicesAdapter(List<Device> devices){
        this.devices = devices;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_single_devices, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Device device = devices.get(position);
        holder.name.setText(device.getName());
        if(device.isChecked()){
            holder.choosedDevice.setChecked(true);
        }
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.isInEditMode() == false){
                    device.setName(holder.name.getText().toString());

                }
            }
        });
        if(device.getState() == 1){
            holder.state.setText("On");
            holder.turnOnOff.setActivated(false);
        }

        else{
            holder.state.setText("Off");
            holder.turnOnOff.setActivated(true);
        }


        holder.turnOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.isActivated()){
                    v.setActivated(false);
                    device.setState(1);
                    device.getManualMode().setState(1);
                    holder.state.setText("On");


                }else{
                    v.setActivated(true);
                    device.setState(0);
                    device.getManualMode().setState(0);
                    holder.state.setText("Off");
                }

                sendDataToAPI(device);

            }
        });
        holder.mode.setText(device.getMode().toString() + " Mode");

        holder.choosedDevice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    for(CheckBox checkBox: checkBoxList){
                        checkBox.setChecked(false);
                    }
                    buttonView.setChecked(true);
                    for(Device d: devices){
                        d.setChecked(false);
                    }
                    DevicesList.getInstance().setCurrentDevice(device);
                    device.setChecked(true);
                }

            }
        });
    }



    private void sendDataToAPI(final Device device) {
        new AsyncTask<Device, Void, String>(){


            @Override
            protected String doInBackground(Device... params) {
                params[0].setMode(Mode.Manual);
                new APIConnection().setManualMode(params[0]);
                new APIConnection().setMode(params[0]);
                return null;
            }
        }.execute(device);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }
}
