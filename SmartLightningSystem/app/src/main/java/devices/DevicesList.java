package devices;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import objects.Mode;

public class DevicesList {

    private static DevicesList instance = null;
    private List<Device> devices = null;
    private Device currentDevice = null;

    public DevicesList(){

    }
    public static DevicesList getInstance(){
        if(instance == null){
            instance = new DevicesList();
        }
        return instance;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public Device getDeviceById(int id){
        if(devices.size()>0){
            for(Device device: devices)
                if(device.getId() == id)
                    return device;
        }
        return null;
    }

    public void setDevices(List<Device> devicesList) {
        if(devices==null){
            devices = new ArrayList<>();
        }
        devices.clear();
        for(Device device: devicesList){
            devices.add(new Device(device));
        }
    }

    public void setCurrentDevice(Device currentDevice) {
        currentDevice.setChecked(true);
        this.currentDevice = currentDevice;

    }


    public List<Device> formatJSON(String json) {

        List<Device> devicesList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);
            int nr = jsonArray.length();
            for(int i=0; i<nr; i++){
                JSONObject jsonDevice = jsonArray.getJSONObject(i);
                Device device = new Device();
                device.setId(jsonDevice.getInt("deviceId"));
                device.setState(0);
                device.setName(jsonDevice.getString("deviceName"));
                device.setType(jsonDevice.getInt("type"));
                Mode[] modes = Mode.values();
                device.setMode(modes[jsonDevice.getInt("workMode")]);
                devicesList.add(device);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return devicesList;
    }

    public void setDeviceData(String json, int nr){
        try {
            Device device = devices.get(nr);
            JSONObject jsonObject = new JSONObject(json);
            device.setId(jsonObject.getInt("deviceId"));
            //device.setState(jsonDevice.getInt("state"));
            device.setName(jsonObject.getString("deviceName"));
            device.setType(jsonObject.getInt("type"));
            Mode[] modes = Mode.values();
            device.setMode(modes[jsonObject.getInt("workMode")]);
            JSONArray jsonArray = jsonObject.getJSONArray("profiles");
            int key;
            for(int i=0; i<jsonArray.length(); i++){
                JSONObject jsonDevice = jsonArray.getJSONObject(i);
                key = jsonDevice.getInt("key");
                String jsonData = jsonDevice.getJSONObject("value").toString();
                device.setModeData(jsonData);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public Device getCurrentDevice(){
        if(devices!=null)
            for(Device device: devices)
                if(device.getId() == currentDevice.getId())
                    return device;
        return currentDevice;
    }


}
