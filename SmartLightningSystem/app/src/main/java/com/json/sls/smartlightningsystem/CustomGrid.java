package com.json.sls.smartlightningsystem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.json.sls.smartlightningsystem.R.color.almostwhite;

public class CustomGrid extends BaseAdapter {
    private Context mContext;
    private final String[] description;
    private final int[] Imageid;
    private int grid_layout_id = R.layout.grid_single;
    private int checkedBulb = -1;

    TextView textView;
    ImageView imageView;


    public CustomGrid(Context c,String[] web,int[] Imageid ) {
        mContext = c;
        this.Imageid = Imageid;
        this.description = web;
    }

    public void setCheckedBulb(int checkedBulb) {
        this.checkedBulb = checkedBulb;
    }

    public int getGrid_layout_id() {
        return grid_layout_id;
    }

    public void setGrid_layout_id(int grid_layout_id) {
        this.grid_layout_id = grid_layout_id;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return description.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(getGrid_layout_id(), null);
            textView =  (TextView) grid.findViewById(R.id.grid_text);
            imageView = (ImageView) grid.findViewById(R.id.grid_image);
            textView.setText(description[position]);
            imageView.setImageResource(Imageid[position]);
            if(position == checkedBulb)
                grid.setActivated(true);

        } else {
            grid = (View) convertView;
        }


        return grid;
    }
}
